# FreeSoftwareActivism

Je posterai ici des traductions en français des affiches de
[FreeSoftwareActivism](https://notabug.org/jyamihud/FreeSoftwareActivism). 

Ce sont des affiches de présentation des Logiciels Libres à destination du grand public.

![Aperçu du poster](poster.jpg)